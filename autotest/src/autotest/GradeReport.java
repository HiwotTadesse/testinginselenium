/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autotest;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author hp-
 */
public class GradeReport {
     public static void main(String[] args) throws InterruptedException, FileNotFoundException, UnsupportedEncodingException 
     {
         // Create a new instance of the Firefox driver
         System.setProperty("webdriver.chrome.driver", "C:\\Gecko\\chromedriver.exe");
         WebDriver driver;
         driver = new ChromeDriver();
         WebDriverWait wait = new WebDriverWait(driver, 20);
         //Launch the some site
         driver.get("http://portal.aait.edu.et");
         //*[@id="UserName"]
         //*[@id="Password"]
         //*[@id="home"]/div[2]/div[2]/form/div[4]/div/button
         //html/body/div[2]/div/div[2]/div[1]/div/div/table
         String gradeUrl = "https://portal.aait.edu.et/Grade/GradeReport";
         driver.findElement(By.xpath(".//*[@id=\"UserName\"]")).sendKeys("Atr/5020/09");
         driver.findElement(By.xpath(".//*[@id=\"Password\"]")).sendKeys("7902");
         driver.findElement(By.xpath(".//*[@id=\"home\"]/div[2]/div[2]/form/div[4]/div/button")).click();
         
         driver.navigate().to(gradeUrl); 
         driver.navigate().refresh();
         String gradeReport = driver.findElement(By.xpath(".//html/body/div[2]/div/div[2]/div[1]/div/div/table")).getText();
         System.out.println(gradeReport);
         PrintWriter print = new PrintWriter("C://Grade/gradeReport.txt", "UTF-8") ;
         print.println(gradeReport);
         print.close();
         Thread.sleep(5000000); 
 
        // Close the driver
        driver.quit(); 
     }
    
}
